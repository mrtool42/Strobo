
##************** Simple Strobe ***************##
##**          Mario Rammelmüller            **##
##**                                        **##
##**             042@aikq.de                **##
##********************************************##

#define LEDPIN            11
#define PULSEPOTI    A0
#define DELAYPOTI     A1

int pulse     =  0;
int cycle  =  0;

void setup() {

pinMode(LEDPIN, OUTPUT);
pinMode(PULSEPOTI, INPUT);
pinMode(DELAYPOTI, INPUT);

}

void loop() {

  pulse = map(analogRead(PULSEPOTI), 0, 1023, 10, 10000);
  cycle = map(analogRead(DELAYPOTI), 0, 1023, 10000, 20000);
  
  digitalWrite(LEDPIN, HIGH);
  delayMicroseconds(pulse);
  digitalWrite(LEDPIN, LOW);
  delayMicroseconds(cycle);

}
